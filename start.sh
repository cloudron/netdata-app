#!/bin/bash

set -eu

echo "==> Ensure directories"
mkdir -p /run/netdata/lock \
        /app/data/etc \
        /app/data/cache \
        /app/data/lib \
        /app/data/plugins \
        /app/data/health.d \
        /app/data/registry
#        /app/data/log

touch /app/data/etc/.opt-out-from-anonymous-statistics

if [[ ! -f /app/data/etc/netdata.conf ]]; then
  echo "==> First run"
  cp -a /etc/netdata/* /app/data/etc/
  cat > /app/data/etc/netdata.conf <<EOT
[global]
  run as user = cloudron
  hostname = ${CLOUDRON_APP_DOMAIN}
[directories]
  config = /app/data/etc
#  log = /app/data/log
  cache = /app/data/cache
  lib = /app/data/lib
  lock = /run/netdata/lock
  plugins = "/usr/libexec/netdata/plugins.d" /app/data/plugins
  health config = /app/data/health.d
  registry = /app/data/registry
EOT
fi

echo "==> Update config"
sed -i -e "s/hostname =.*/hostname = ${CLOUDRON_APP_DOMAIN}/" /app/data/etc/netdata.conf

echo "==> Changing ownership"
chown -R cloudron:cloudron /app/data

echo "==> Starting OpenHAB"
exec gosu cloudron:cloudron /usr/sbin/netdata -c /app/data/etc/netdata.conf -u netdata -D
