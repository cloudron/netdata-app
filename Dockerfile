FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code /app/pkg/

ARG VERSION=1.44.3

RUN apt-get update && \
    apt-get install -y libuv1-dev && \
    rm -rf /var/cache/apt /var/lib/apt/lists

WORKDIR /app/code
RUN curl -L https://github.com/netdata/netdata/releases/download/v${VERSION}/netdata-v${VERSION}.tar.gz | \
    tar xz --strip-components 1 -C /app/code

RUN chmod +x netdata-installer.sh && \
    ./netdata-installer.sh --dont-wait --dont-start-it --use-system-protobuf --disable-ebpf --install-no-prefix / --stable-channel --disable-telemetry

# https://learn.netdata.cloud/docs/netdata-agent/configuration/anonymous-telemetry-events
ENV DISABLE_TELEMETRY 1

# https://learn.netdata.cloud/docs/netdata-agent/configuration/daemon-configuration#logs-section-options
RUN mkdir -p /var/log/netdata && \
    ln -sf /dev/stdout /var/log/netdata/access.log && \
    ln -sf /dev/stdout /var/log/netdata/aclk.log && \
    ln -sf /dev/stdout /var/log/netdata/debug.log && \
    ln -sf /dev/stderr /var/log/netdata/error.log && \
    ln -sf /dev/stderr /var/log/netdata/daemon.log && \
    ln -sf /dev/stdout /var/log/netdata/collector.log && \
    ln -sf /dev/stdout /var/log/netdata/fluentbit.log && \
    ln -sf /dev/stdout /var/log/netdata/health.log

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]
